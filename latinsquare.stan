

functions {
    // Evaluate prior defined through spline
    real eval_spline2_lpdf(real logitW, vector x, matrix[] y, int idx, int Nres){
        real dx;
        real val;
        int idd;
        int low;
        int high;
        int med;
        
        // Search for correct cubic polynomial
        low = 1;
        high = Nres;
        while(high-low > 1){
            med = (high+low)/2;
            if(logitW < x[med]){
                high = med;
            } else{
                low = med;
            }
        }
        idd = low;
        dx = logitW-x[idd];
        val =  y[idx, idd, 1];
        val += y[idx, idd, 2]*dx;
        val += y[idx, idd, 3]*dx*dx;
        val += y[idx, idd, 4]*dx*dx*dx;
    
        return(val);
    }
  
    // include real lambda/a/b as input if these values should not be pre-decided
    
    // inverse gamma on log variance (input to function is log variance, and we calculate log)
    real invgamma_logvar_lpdf(real logvar, real a, real b){
        real val;
        // Calculate prior on precision
        val = gamma_lpdf(exp(-logvar) | a, b);
        // Add jacobian
        val += -logvar;
        return(val);
    }
    real hc_logx_lpdf(real x){ // on log variance
        real lambda = 25;
        return -log(pi()) - log(lambda) - log(exp(-x/2) + exp(x/2)/lambda^2);
    }
    real pc_logx_lpdf(real x){ // on log variance
        real lambda = -log(0.05)/3;
        return log(lambda/2) + x/2 - lambda*exp(x/2);
    }
    real jeffrey_logx_lpdf(real x){ // on log variance
      return 0;
    }
    
    real prior_lpdf(real x, int number){
        if (number == 1){
            return invgamma_logvar_lpdf(x | 1, 5e-5);
        } else if (number == 2){
            return hc_logx_lpdf(x);
        } else {
            return pc_logx_lpdf(x);
        }
    }
  
}

data {
    // Which prior to use
    int prior_number; // 1 = inla default, 2 = hc(25), 3 = pc(3, 0.05), 4 = new PC prior
  
    // Data and design
    vector[81] y; // observations
    int idxC[81]; // column index
    int idxR[81]; // row index
    int idxT[81]; // treatment index

    // Spline for new PC prior
    int Nres;
    matrix[Nres, 4] priors[4];
    vector[Nres] logitWs;
    int order;
    
    int LOO; // for the leave one out cv
    int notLO[80];
    
    // Sample from prior?
    int use_likelihood;
    
}

parameters {
    // Parameters: log(var) for prior_num = 1,2,3 and internal weight par for prior_num = 4
    real thetaN[5];
  
    // Random effects
    vector[9] col_iid; // iid column effect
    vector[9] row_iid; // iid row effect
    vector[9] treatment_iid; // iid treatment effect
    vector[9] treatment_rw2; // rw2 treatment effect
}

transformed parameters {
    // Variances
    real var_col;
    real var_row;
    real var_tr_iid;
    real var_tr_rw2;
    real var_noise;
    real var_total;
    
    // Weights
    real vLat;
    real vLat_c;
    real vLat_tr;
    real vLat_tr_c;
    real vLat_tr_rw2;
    real vLat_tr_rw2_c;
    real vLat_ab_a;
    real vLat_ab_a_c;
    
    real theta[5];
    // Total weight: 1 = row, 2 = col, 3 = tr_rw2, 4 = tr_iid, 5 = noise
    real w[5];
    
    // Linear predictor
    vector[81] eta;
  
    // RW2 projected to non-singular subspace
    vector[9] treatment_rw2_new;
    vector[9] vec;
  
    // Project RW2 away from null space
    for (i in 1:9){
        vec[i] = i-5;
    }
    treatment_rw2_new = treatment_rw2 - mean(treatment_rw2) - sum(treatment_rw2 .* vec) / (sum(vec .* vec)) * vec;
    
    // Set theta to be internal thetaN parameter
    for(i in 1:5){
        theta[i] = thetaN[i];
    }
    
    // Extract parameters
    if (prior_number != 4) {
        // Get variances
        var_row    = exp(theta[1]);
        var_col    = exp(theta[2]);
        var_tr_rw2 = exp(theta[3]);
        var_tr_iid = exp(theta[4]);
        var_noise  = exp(theta[5]);
        var_total  = var_col + var_row + var_tr_iid + var_tr_rw2 + var_noise;
        
        // Convert to weights
        vLat = (var_col + var_row + var_tr_iid + var_tr_rw2)/var_total;
        vLat_c = var_noise/var_total;
        vLat_tr = (var_tr_iid + var_tr_rw2)/(var_col + var_row + var_tr_iid + var_tr_rw2);
        vLat_tr_c = (var_col + var_row)/(var_col + var_row + var_tr_iid + var_tr_rw2);
        vLat_tr_rw2 = var_tr_rw2/(var_tr_rw2 + var_tr_iid);
        vLat_tr_rw2_c = var_tr_iid/(var_tr_rw2 + var_tr_iid);
        vLat_ab_a = var_row/(var_row + var_col);
        vLat_ab_a_c = var_col/(var_row + var_col);
    } else {
        // Introduce non-linear changes to logit(w) to control tails
        if(thetaN[3] > 0.5){
            theta[3] = 0.25 + thetaN[3]^2;
        }
        
        // Get weights
        vLat        = inv_logit(theta[1]);
        if(theta[1] > 34){
            vLat_c = exp(-theta[1]);
        } else{
            vLat_c = 1-vLat;
        }
        vLat_tr     = inv_logit(theta[2]);
        if(theta[2] > 34){
            vLat_tr_c = exp(-theta[2]);
        } else{
            vLat_tr_c = 1-vLat_tr;
        }
        vLat_tr_rw2 = inv_logit(theta[3]);
        if(theta[3] > 34){
            vLat_tr_rw2_c = exp(-theta[3]);
        } else{
            vLat_tr_rw2_c = 1-vLat_tr_rw2;
        }
        vLat_ab_a   = inv_logit(theta[4]);
        if(theta[4] > 34){
            vLat_ab_a_c = exp(-theta[4]);
        } else{
            vLat_ab_a_c = 1-vLat_ab_a;
        }
        var_total   = exp(theta[5]);
    }
    
    // Get total weight
    if(order == 1){
        w[1] = vLat*vLat_tr_c*vLat_ab_a;
        w[2] = vLat*vLat_tr_c*vLat_ab_a_c;
        w[3] = vLat*vLat_tr*vLat_tr_rw2;
        w[4] = vLat*vLat_tr*vLat_tr_rw2_c;
        w[5] = vLat_c;
    } else if(order == 2){
        w[1] = vLat*vLat_tr;
        w[2] = vLat*vLat_tr_c*vLat_ab_a;
        w[3] = vLat*vLat_tr_c*vLat_ab_a_c*vLat_tr_rw2;
        w[4] = vLat*vLat_tr_c*vLat_ab_a_c*vLat_tr_rw2_c;
        w[5] = vLat_c;
    } else if(order == 3){
        w[1] = vLat*vLat_tr_c*vLat_ab_a_c;
        w[2] = vLat*vLat_tr;
        w[3] = vLat*vLat_tr_c*vLat_ab_a*vLat_tr_rw2;
        w[4] = vLat*vLat_tr_c*vLat_ab_a*vLat_tr_rw2_c;
        w[5] = vLat_c;
    }
    
    // Calculate variances
    var_row    = w[1]*var_total;
    var_col    = w[2]*var_total;
    var_tr_rw2 = w[3]*var_total;
    var_tr_iid = w[4]*var_total;
    var_noise  = w[5]*var_total;
    
    // Create linear predictor
    for(i in 1:81){
        eta[i] = 0;
    }
    if(var_row > 1e-300){
        eta += sqrt(var_row)*row_iid[idxR];
    }
    if(var_col > 1e-300){
        eta += sqrt(var_col)*col_iid[idxC];
    }
    if(var_tr_rw2 > 1e-300){
        eta += sqrt(var_tr_rw2)*treatment_rw2_new[idxT];
    }
    if(var_tr_iid > 1e-300){
        eta += sqrt(var_tr_iid)*treatment_iid[idxT];
    }
}

model {
    real cFac;
    
    // Evaluate priors
    if (prior_number != 4) {
        // Variances of random effects
        target += prior_lpdf(theta[1] | prior_number);
        target += prior_lpdf(theta[2] | prior_number);
        target += prior_lpdf(theta[3] | prior_number);
        target += prior_lpdf(theta[4] | prior_number);
        
        // Likelihood variance
        target += jeffrey_logx_lpdf(theta[5]); // jeffreys prior on noise variance
    } else {
        if(thetaN[3] < 0.5){
            cFac = 0;
        } else{
            cFac = log(2*thetaN[3]);
        }
        // Distribution of variance relative to total variance
        target += eval_spline2_lpdf(theta[1] | logitWs, priors, 1, Nres);
        target += eval_spline2_lpdf(theta[2] | logitWs, priors, 2, Nres);
        target += eval_spline2_lpdf(theta[3] | logitWs, priors, 3, Nres) + cFac;
        target += eval_spline2_lpdf(theta[4] | logitWs, priors, 4, Nres);
        
        // Total variance
        target += jeffrey_logx_lpdf(theta[5]);
    }
  
    // random iid effects
    target += normal_lpdf(col_iid | 0, 1);
    target += normal_lpdf(row_iid | 0, 1);
    target += normal_lpdf(treatment_iid | 0, 1);
  
    // random walk effect
    target += -0.5 * 1.339018 * dot_self(treatment_rw2[1:7] - 2*treatment_rw2[2:8] + treatment_rw2[3:9]);
    target += normal_lpdf(sum(treatment_rw2) | 0, 1*sqrt(9)); // (soft) sum to zero-constraint on rw2
    target += normal_lpdf(dot_product(treatment_rw2, vec) | 0, 1*sqrt(60)); // (soft) linear sum to zero-constraint on rw2, 60 = sum(((1:9)-5)^2)
  
    // data model
    if (use_likelihood == 1) {
      if (LOO > 0){
        target += normal_lpdf(y[notLO] | eta[notLO], sqrt(var_noise));
      } else {
        target += normal_lpdf(y | eta, sqrt(var_noise));
      }
    } else {
        target += normal_lpdf(theta[5] | 0, 1);
    }
}

generated quantities {
  
    // Get random effects
    vector[9] row_effect = sqrt(var_row) * row_iid;
    vector[9] col_effect = sqrt(var_col) * col_iid;
    vector[9] treat_rw2_effect = sqrt(var_tr_rw2) * treatment_rw2_new;
    vector[9] treat_iid_effect = sqrt(var_tr_iid) * treatment_iid;
    
    // calculate log score
    real log_pred_dens;
    if (LOO > 0){
      log_pred_dens = normal_lpdf(y[LOO] | eta[LOO], sqrt(var_noise));
    } else {
      log_pred_dens = 0;
    }
    
}




