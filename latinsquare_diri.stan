

functions {
  real jeffrey_logx_lpdf(real x){ // on log variance
      return 0;
    }
        // Evaluate prior defined through spline
    real eval_spline2_lpdf(real logitW, vector x, matrix[] y, int idx, int Nres){
        real dx;
        real val;
        int idd;
        int low;
        int high;
        int med;
        
        // Search for correct cubic polynomial
        low = 1;
        high = Nres;
        while(high-low > 1){
            med = (high+low)/2;
            if(logitW < x[med]){
                high = med;
            } else{
                low = med;
            }
        }
        idd = low;
        dx = logitW-x[idd];
        val =  y[idx, idd, 1];
        val += y[idx, idd, 2]*dx;
        val += y[idx, idd, 3]*dx*dx;
        val += y[idx, idd, 4]*dx*dx*dx;
    
        return(val);
    }
    
    real gamma_logx_lpdf(real x, real a){ // gamma distribution on log variance
        real b = 1;
        return a*log(b) - lgamma(a) + a*x - b*exp(x);
    }
}

data {
  
  // Which prior to use
    int prior_number; // 1 = inla default, 2 = hc(25), 3 = pc(3, 0.05), 4 = new PC prior, 5 = dirichlet + PC combo
    int diri_order; // 3 = diri between 3, 4 = diri between 4, 5 = diri between 5, only used if prior_number == 5
  
    // Data and design
    vector[81] y; // observations
    int idxC[81]; // column index
    int idxR[81]; // row index
    int idxT[81]; // treatment index

    // Spline for new PC prior
    int Nres;
    matrix[Nres, 4] priors[4];
    vector[Nres] logitWs;
    int order;
    
    int LOO; // for the leave one out cv
    int notLO[80];

    // Sample from prior?
    int use_likelihood;
  
}

parameters {
  
    // totvar, 5 weights, internal scale
    real thetaNN[5];
    real logV; // log total variance
  
    // Random effects
    vector[9] col_iid; // iid column effect
    vector[9] row_iid; // iid row effect
    vector[9] treatment_iid; // iid treatment effect
    vector[9] treatment_rw2; // rw2 treatment effect
  
}

transformed parameters {
  
    // Total weight: 1 = row, 2 = col, 3 = tr_rw2, 4 = tr_iid, 5 = noise
    real w_r;
    real w_c;
    real w_tr;
    real w_ti;
    real w_eps;
    
    real var_col;
    real var_row;
    real var_tr_iid;
    real var_tr_rw2;
    real var_noise;
    
    // Linear predictor
    vector[81] eta;
    
    real totvar;
    vector[5] thetaN; // scale the weight to rw2
    vector[5] theta; // logit total weights (prior == 4 or 5) or variances (prior <= 3)
    vector[diri_order] v; // weights to the dirichlet
    vector[diri_order] vars; // exp(thetaN) that are sampled from gamma
  
    // RW2 projected to non-singular subspace
    vector[9] treatment_rw2_new;
    vector[9] vec;
    
    // Project RW2 away from null space
    for (i in 1:9) vec[i] = i-5;
    treatment_rw2_new = treatment_rw2 - mean(treatment_rw2) - sum(treatment_rw2 .* vec) / (sum(vec .* vec)) * vec;
  
    for(i in 1:5) thetaN[i] = thetaNN[i];
  
  if (prior_number == 5){
    if (diri_order == 3){
      
      // Introduce non-linear changes to logit(w) to control tails
      if(thetaNN[4] > 0.5) thetaN[4] = 0.25 + thetaNN[4]^2;
      
      // have sampled gamma variables, so now we are scaling them to be dirichlet
      vars = to_vector(exp(thetaN[1:3]));
      v = vars/sum(vars);
      
      theta[5] = -thetaN[5]; // theta[5] is amount to residuals, thetaN[5] is amount to latent model
      theta[1:2] = logit((1-inv_logit(theta[5])) * v[1:2]); // (1 - w_residual) * weight_(row/col)
      theta[3] = logit((1-inv_logit(theta[5])) * v[3] * inv_logit(thetaN[4])); // (1 - w_residual) * weight_treatment * weight_rw2
      if(thetaN[4] < 31){
        theta[4] = logit((1-inv_logit(theta[5])) * v[3] * (1-inv_logit(thetaN[4]))); // (1 - w_residual) * weight_treatment * (1-weight_rw2)
      } else{
        theta[4] = -1000;
      }
    } else if (diri_order == 4){
      
      vars = to_vector(exp(thetaN[1:4]));
      v = vars/sum(vars);
      
      theta[5] = -thetaN[5]; // theta[5] is amount to residuals, thetaN[5] is amount to latent model
      theta[1:4] = logit(v[1:4] * (1-inv_logit(theta[5]))); // weight to each of the 4 components, times 1-residual weight
      
    } else {
      vars = to_vector(exp(thetaN));
      v = vars/sum(vars);
      theta = logit(v);
    }
  }
  eta = rep_vector(0, 81);
  
  totvar = exp(logV);
  w_r = inv_logit(theta[1]);
  w_c = inv_logit(theta[2]);
  w_tr = inv_logit(theta[3]);
  w_ti = inv_logit(theta[4]); // sett lik 0 om ikke inv_logit(-1000) blir 0
  w_eps = inv_logit(theta[5]);
  
  var_row    = w_r*totvar;
  var_col    = w_c*totvar;
  var_tr_rw2 = w_tr*totvar;
  var_tr_iid = w_ti*totvar;
  var_noise  = w_eps*totvar;
  
  if (w_r > 1e-300) eta += row_iid[idxR]* sqrt(var_row);
  if (w_c > 1e-300) eta += col_iid[idxC]* sqrt(var_col);
  if (w_tr > 1e-300) eta += treatment_rw2_new[idxT]* sqrt(var_tr_rw2);
  if (w_ti > 1e-300) eta += treatment_iid[idxT]* sqrt(var_tr_iid);
  
}

model {
  
    real cFac4;
    cFac4 = 0;
    
    if (diri_order == 3){
      
      // thetaN[1:3] are log variances, 
      // thetaN[4] is logit weight of proportion to structured in split between structured and unstructured treatment,
      // thetaN[5] is logit weight of proportion to latent (NOT to residuals)
      target += gamma_logx_lpdf(thetaN[1] | 0.754);
      target += gamma_logx_lpdf(thetaN[2] | 0.754);
      target += gamma_logx_lpdf(thetaN[3] | 0.754);
      
      if(thetaNN[4] < 0.5) cFac4 = 0; else cFac4 = log(2*thetaNN[4]);
      
      target += cFac4; // avoid numerical instabilities
      
      target += eval_spline2_lpdf(thetaN[4] | logitWs, priors, 3, Nres); // PC on split between structured and unstructured
      target += eval_spline2_lpdf(thetaN[5] | logitWs, priors, 1, Nres); // PC on residual variance
      
    } else if (diri_order == 4){
      
      // thetaN[1:4] are log variances, 
      // thetaN[5] is logit weight of proportion to latent (NOT to residuals)
      target += gamma_logx_lpdf(thetaN[1] | 0.683);
      target += gamma_logx_lpdf(thetaN[2] | 0.683);
      target += gamma_logx_lpdf(thetaN[3] | 0.683);
      target += gamma_logx_lpdf(thetaN[4] | 0.683);
      
      target += eval_spline2_lpdf(thetaN[5] | logitWs, priors, 1, Nres); // PC on residual variance, 1-weight to residuals
      
    } else {
      
      // thetaN are all log variances
      target += gamma_logx_lpdf(thetaN[1] | 0.649);
      target += gamma_logx_lpdf(thetaN[2] | 0.649);
      target += gamma_logx_lpdf(thetaN[3] | 0.649);
      target += gamma_logx_lpdf(thetaN[4] | 0.649);
      target += gamma_logx_lpdf(thetaN[5] | 0.649);
      
    }
    
    target += jeffrey_logx_lpdf(logV);
    
    // random iid effects
    target += normal_lpdf(col_iid | 0, 1);
    target += normal_lpdf(row_iid | 0, 1);
    target += normal_lpdf(treatment_iid | 0, 1);
  
    // random walk effect
    target += -0.5 * 1.339018 * dot_self(treatment_rw2[1:7] - 2*treatment_rw2[2:8] + treatment_rw2[3:9]);
    target += normal_lpdf(sum(treatment_rw2) | 0, 1*sqrt(9)); // (soft) sum to zero-constraint on rw2
    target += normal_lpdf(dot_product(treatment_rw2, vec) | 0, 1*sqrt(60)); // (soft) linear sum to zero-constraint on rw2, 60 = sum(((1:9)-5)^2)
    
    
    // data model
    if (use_likelihood == 1) {
        if (LOO > 0){
        target += normal_lpdf(y[notLO] | eta[notLO], sqrt(var_noise));
      } else {
        target += normal_lpdf(y | eta, sqrt(var_noise));
      }
    } else {
        target += normal_lpdf(logV | 0, 1);
    }
  
}

generated quantities {
      // Get random effects
    vector[9] row_effect = sqrt(var_row) * row_iid;
    vector[9] col_effect = sqrt(var_col) * col_iid;
    vector[9] treat_rw2_effect = sqrt(var_tr_rw2) * treatment_rw2_new;
    vector[9] treat_iid_effect = sqrt(var_tr_iid) * treatment_iid;
  
    // // Calculate marginal log-likelihoods
    // vector[81] log_lik;
    // for (n in 1:81){
    //     log_lik[n] = normal_lpdf(y[n] | eta[n], sqrt(var_noise));
    // }
    
    // calculate log score
    real log_pred_dens;
    if (LOO > 0){
      log_pred_dens = normal_lpdf(y[LOO] | eta[LOO], sqrt(var_noise));
    } else {
      log_pred_dens = 0;
    }
    
}

